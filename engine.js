var currentComponentId;
var currentComponent;

var currentPageId = 0;
var currentPage = null;

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var app = angular.module('show_page', []);
app.controller('leftCtrl', function($scope) {

});


app.controller('contentCtrl', function($scope, $rootScope) {

});

app.controller('textController', ['$scope', '$rootScope', TextController]);

app.controller('imgController', ['$scope', '$rootScope', ImgController]);


function BaseComponentController() {

    this.currentComponent = undefined;

    //改变属性值响应事件
    this.changeX = function() {
        console.log("x:" + this.currentComponent.x);

    };

    this.changeY = function() {
        console.log("y:" + this.currentComponent.y);

    };

    this.changeWidth = function() {
        console.log("width:" + this.currentComponent.width);

    };

    this.changeHeight = function() {
        console.log("height:" + this.currentComponent.height);

    };

    //点击某个组件触发初始化
    this.init = function() {
        currentComponent = {
            x: Math.floor(Math.random() * 10),
            y: 100,
            width: 20,
            height: 30
        };
        this.currentComponent = currentComponent;
    };

    //响应事件: 移动组件时改变属性框的值
    this.update = function() {

    }
}

function TextController($scope, $rootScope) {

    BaseComponentController.call(this);

    this.changeText = function() {
        console.log("text:" + $scope);
    };
}


function ImgController($scope, $rootScope) {

    BaseComponentController.call(this);

    this.changeText = function() {
        console.log("text:" + this.currentComponent.text);

    };
}

var componentPanel = {

    init: function(componentName) {
        $("#init" + componentName.capitalizeFirstLetter()).trigger('click');
    },
    update: function(componentName) {
        $("#update" + componentName.capitalizeFirstLetter()).trigger('click');
    }

}







